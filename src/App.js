import React from 'react';
import Tabs from './Tabs';
import Items from './Items';
import './App.css';

let currentTab = 0
let previousTab = null

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      tabs: [
        { tabId: 1, tabName: 'tab1'},
        { tabId: 2, tabName: 'tab2'},
        { tabId: 3, tabName: 'tab3'},
        { tabId: 4, tabName: 'tab4'}
      ],
      items: [
        {itemId: 2, itemName: 'item21'},
        {itemId: 4, itemName: 'item41'},
        {itemId: 3, itemName: 'item32'},
        {itemId: 2, itemName: 'item23'},
        {itemId: 1, itemName: 'item11'},
        {itemId: 1, itemName: 'item1'},
        {itemId: 3, itemName: 'item33'},
        {itemId: 2, itemName: 'item22'},
        {itemId: 4, itemName: 'item48'},
        {itemId: 1, itemName: 'item12'},
        {itemId: 1, itemName: 'item13'},
        {itemId: 2, itemName: 'item27'}
      ],
      sortedItemsById: []
    }
  }
 
  sortArrayByCategory = (e) => {
    const selectedTab = e.target.tabIndex
    currentTab = previousTab 
    if(currentTab !== selectedTab) {
      currentTab = selectedTab 
    }
    const clickedTab = this.state.items.filter(item => item.itemId === selectedTab)
    this.setState({
      sortedItemsById: clickedTab
    })
  }

  render() {
    return (
      <div className="app">
        <header className="app-header">
          <>
           <Tabs tabs={this.state.tabs} sortArrayByCategory={this.sortArrayByCategory} />
          </>
        </header>
        <>
          <Items sortedItemsById={this.state.sortedItemsById}/>
        </>
      </div>
    );
  }
}

export default App;
