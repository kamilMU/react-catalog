import React from 'react'

function Items ({sortedItemsById}) {
    return (
        <>
         <ul className={"products-container"}>
            {sortedItemsById.map(item =>
                <li className="products-container__item" key={item.itemName}>
                <img src='https://picsum.photos/200'alt="" />
                <span className="products-container__item-name">{item.itemName}</span>
                </li>
            )}
         </ul>
        </>
    )
}

export default Items