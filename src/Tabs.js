import React from 'react'

function Tabs ({tabs, sortArrayByCategory}) {
    return (
        <>
         <ul className="tabs-container">
            {tabs.map(tab => 
            <li className="tabs-container__tab-name" onClick={sortArrayByCategory} key={tab.tabId}>
                <a href="#" tabIndex={tab.tabId}>{tab.tabName}</a>
            </li>
            )}
         </ul>
        </>
    )
}

export default Tabs